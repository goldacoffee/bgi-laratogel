<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::get('/hitunghasil', function () {
    Artisan::call('togel:hitunghasil');
    Artisan::output();
});

Route::get('/hitunghasil/{tanggal}/{keluar}', function ($tanggal, $keluar) {


    Artisan::call('togel:hitung2D', [
        'tanggal' => $tanggal,
        'keluar' => $keluar,
    ]);
    Artisan::output();
});
Route::get('/hitungdukun/{provider}/{tanggal}/{keluar}', function ($provider, $tanggal, $keluar) {


    Artisan::call("togel:dukun", [
        'provider' => $provider,
        'tanggal' => $tanggal,
        'keluar' => $keluar,
    ]);
    Artisan::output();
});

// dukun togel
Route::group(['prefix' => 'dukuntogel'], function () {

    Route::get('tebakan', 'DukunController@tebakanUser')->name('dukun.tebakanuser');
    Route::post('periode', 'DukunController@addPeriode')->name('dukun.addperiode');
    Route::get('periode', 'DukunController@getPeriodeList')->name('dukun.getpierodelist');
    Route::get('periode/{uuid}', 'DukunController@getPeriodeDetails')->name('dukun.getperiodedetails');
    Route::delete('periode/{uuid}', 'DukunController@deletePeriodeDetails')->name('dukun.deleteperiodedetails');


    Route::get('/klasemen/{userid?}', 'DukunHasilController@klasemen');
    Route::get('/miniklasemen', 'DukunHasilController@miniKlasemen');
    Route::get('/snk', 'DukunHasilController@getSNK');
    Route::get('/custommsg', 'DukunHasilController@custommsg');
    Route::get('/hapusPeriode', 'DukunHasilController@simulasiHapusPeriode');
});


Route::group(['prefix' => 'user'], function () {

    Route::post('changepass', 'ApiController@changePassword');
    Route::post('add', 'ApiController@addUser');
    Route::get('list', 'ApiController@listUser');
    Route::post('details', 'ApiController@userDetails');
    Route::post('delete', 'ApiController@userDelete');
    Route::post('deleteAll', 'ApiController@deleteAll');

    Route::post('disableAll', 'ApiController@disableAll');
    Route::post('enableAll', 'ApiController@enableAll');
    Route::post('enableUser', 'ApiController@enableUser');
    Route::post('disableUser', 'ApiController@disableUser');
});

Route::group(['prefix' => 'banner'], function () {
    Route::get('/details/{location}', 'ApiController@bannerDetails');
    Route::post('add', 'ApiController@bannerAdd');
});

Route::group(['prefix' => 'sidebar'], function () {
    Route::get('details/{id}', 'ApiController@detailSidebar');
    Route::post('add', 'ApiController@addSidebar');
    Route::post('delete', 'ApiController@deleteSidebar');
    Route::get('list', 'ApiController@listSidebar');
});

Route::post('imageUpload', 'ApiController@fileUpload');
Route::get('imageUpload', 'ApiController@fileUploadGet');

Route::group(['prefix' => 'page'], function () {


    Route::post('add', 'ApiController@addPage');
    Route::post('delete', 'ApiController@deletePage');
    Route::get('list', 'ApiController@listPage');
    Route::get('formenu', 'ApiController@pageForMenu');
    Route::get('details/{id}', 'ApiController@detailPage');
    Route::get('allowed', 'ApiController@allowedProvider');
});

Route::group(['prefix' => 'menu'], function () {
    Route::post('add', 'ApiController@addMenu');

    Route::get('list', 'ApiController@listMenu');
    Route::post('update', 'ApiController@updateMenu');
    Route::post('delete', 'ApiController@deleteMenu');
    Route::get('details/{id}', 'ApiController@detailsMenu');
});




Route::group(['prefix' => 'settings'], function () {

    Route::get('/custommenu', 'ApiController@customMENU');
    Route::post('/custommenu', 'ApiController@customMENUUpdate');
    Route::get('/customcss', 'ApiController@customCSS');
    Route::post('/customcss', 'ApiController@customCSSUpdate');
    Route::get('/customjs', 'ApiController@customJS');
    Route::post('/customjs', 'ApiController@customJSUpdate');
    Route::get('/list', 'ApiController@list');
    Route::post('/add', 'ApiController@add');
    Route::post('/update', 'ApiController@update');
    Route::post('/delete', 'ApiController@delete');
    Route::get('/custom-lomba', 'ApiController@customLombaGet');
    Route::post('/custom-lomba', 'ApiController@customLombaPost');
    Route::get('/custom-dukun', 'ApiController@customDukunGet');
    Route::post('/custom-dukun', 'ApiController@customDukunPost');
});


Route::resource('rekomendasi', 'TogelRekomendasiController');


Route::get('/periode/custommsg', 'PeriodeTebakController@custommsg');
Route::get('/periode/klasemen', 'PeriodeTebakController@getKlasemen');
Route::get('/periode/snk', 'PeriodeTebakController@getSNK');
Route::get('/periode/katapengantar', 'PeriodeTebakController@getKatapengantar');
Route::get('/periode/result', 'PeriodeTebakController@result');
Route::get('/periode/hitunghasil', 'PeriodeTebakController@hitungHasil');

Route::resource('periode', 'PeriodeTebakController');



Route::get('livedrawBanner', function () {


    try {
        $header =  loadBanner('livedraw-atas');
        $footer =  loadBanner('livedraw-bawah');
        return response([
            'success' => true,
            'data' => [
                'header' => $header,
                'footer' => $footer
            ]
        ], 200);
    } catch (\Throwable $th) {
        return response([
            'success' => false,

        ], 201);
    }
});


Route::get('livedraw/{kode}', function ($kode) {

    // return $kode'
    try {
        $data =  file_get_contents('https://livedraw.gustiarma.com/?country=' . $kode);
        // $data =  file_get_contents(public_path() . '/json/sgp.html');
        // $data = json_decode($data, false);
        return response([
            'success' => true,
            'data' => $data
        ], 200);
    } catch (\Throwable $th) {
        return response($th->getMessage(), 201);
    }
});
