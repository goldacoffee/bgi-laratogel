import jQuery from 'jquery'
import moment from 'moment-timezone'
moment.tz.setDefault('Asia/Jakarta')
moment.locale('id')
window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

// assign


window.$ = jQuery
import Vue from 'vue'







if (document.querySelector(".result-date")) {
  document.querySelector(".result-date").parentElement.classList.add("latest");
}

// function toggleLuckyModal() {
//   var luckModal = $("#lucky-modal");
//   if (luckModal.hasClass("showlucky")) {
//     luckModal.removeClass("showlucky");
//     luckModal.addClass("hidelucky");
//   } else {
//     $("#lucky-input").focus();
//     luckModal.removeClass("hidelucky");
//     luckModal.addClass("showlucky");
//   }
// }

// function userVote(choice) {
//   $.post("/api/vote", { choiceId: parseInt(choice) }, function (err, data) {
//     if (err) {
//       console.log(err);
//     }
//     // localStorage.setItem("user-vote",);
//   });
// }

// MOBILE NAV TOGGLE
$(function () {
  $(".open-mobilenav").click(function () {
    $(".mobileoverlay").toggleClass("open");
    setTimeout(function () {
      $("body").css("overflow", "hidden");
    }, 300); //timeout should match css transition time for .mobilenav.open
  });
  $(".close-mobilnav").click(function () {
    $(".mobileoverlay").toggleClass("open");
    $("body").css("overflow", "initial");
  });
});
$(window).resize(function () {
  if ($(window).width() > 960 && $(".mobileoverlay").hasClass("open")) {
    $(".mobileoverlay").removeClass("open");
    $("body").css("overflow", "initial");
    // console.log('more than 1024');
  }
});


$(document).ready(function () {
  $(".trigger_popup_fricc").click(function () {
    $('.hover_bkgr_fricc').show();
  });
  // $('.hover_bkgr_fricc').click(function () {
  //   $('.hover_bkgr_fricc').hide();
  // });
  $(".popupCloseButton").click(function () {
    // console.log('terklik');
    $('.hover_bkgr_fricc').hide();
  });


  $("button.surveybutton").on("click", function () {
    sendVote();
  });
});

(function blink () {
  $(".blink_me")
    .fadeOut(500)
    .fadeIn(500, blink);
})();




Vue.filter('formatTanggal', function (value) {
  if (value) {
    return moment(String(value)).format('DD-MM-YYYY')
  }
});
Vue.filter('formatTanggal2', function (value) {
  if (value) {
    return moment(String(value)).format('DD/MM')
  }
});




import RekomendasiBandar from './ExternalComponent/RekomendasiBandar';
import Klasemen from './components/tebaknomor/Klasemen'
import MiniKlasemen from './components/tebaknomor/MiniKlasemen'
import BukuMimpi from './ExternalComponent/BukuMimpi';
import IsiPrediksi from './ExternalComponent/IsiPrediksi';
import SNKetentuan from './ExternalComponent/SNK'
import SDKDukun from './ExternalComponent/DukunTogel/SNKDukun'
import KataPengantar from './ExternalComponent/KataPengantar'
import LiveDraw from './ExternalComponent/LiveDraw'

import DukunTogel from './ExternalComponent/DukunTogel/'

import DukunKlasemen from './ExternalComponent/DukunTogel/Klasemen'
import DukunKlasemenMini from './ExternalComponent/DukunTogel/MiniKlasemen'


Vue.component(IsiPrediksi.name, IsiPrediksi)
Vue.component(RekomendasiBandar.name, RekomendasiBandar)
Vue.component(Klasemen.name, Klasemen)
Vue.component(MiniKlasemen.name, MiniKlasemen)
Vue.component(SNKetentuan.name, SNKetentuan)
Vue.component(SDKDukun.name, SDKDukun)
Vue.component(KataPengantar.name, KataPengantar)
Vue.component(LiveDraw.name, LiveDraw)
Vue.component(DukunTogel.name, DukunTogel)
Vue.component(DukunKlasemen.name, DukunKlasemen)
Vue.component(DukunKlasemenMini.name, DukunKlasemenMini)


const app = new Vue({
  el: '#app',

  components: {
    BukuMimpi,
  }

});
