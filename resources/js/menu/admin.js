const adminmenu = [
    {
        name: "Pengguna",
        route: "manajemenUserList"
    },
    {
        name: "Pengaturan",
        route: "manajemenSitus"
    },
    {
        name: "Sidebar",
        route: "manajemenSidebarList"
    },
    {
        name: "Menu",
        // route: "manajemenMenuList",
        submenu: [
            {
                name: "Atur menu",
                route: "manajemenMenuList",
            },
            {
                name: "Tambah Menu",
                route: "manajemenMenuAdd",
            }

        ]
    },
    {
        name: "Halaman",
        // route: "manajemenPageList"
        submenu: [
            {
                name: "List Page",
                route: "manajemenPageList",
            },
            {
                name: "Tambah Page",
                route: "manajemenPageAdd",
            }

        ]
    },
    {
        name: "Custom",
        // route: "manajemenPageList"
        submenu: [
            {
                name: "Custom Menu",
                route: "manajemenCustomMenu",
            },
            {
                name: "Custom CSS",
                route: "manajemenCustomCSS",
            },
            {
                name: "Custom HTML Footer",
                route: "manajemenCustomJS",
            }

        ]
    },
    {
        name: "Banner",
        route: "manajemenBannerIndex"

    },
    {
        name: "Rekomendasi",
        route: "manajemenRekomendasi"

    },
    {
        name: "Gambar",
        route: "manajemenImage"

    },

    {
        name: "Lomba",
        // route: "manajemenPageList"
        submenu: [
            {
                name: "Periode Lomba",
                route: "manajemenPeriode",
            },
            {
                name: "HasilLomba",
                route: "manajemenHasilPeriode",
            },
            {
                name: "Custom Hasil",
                route: "manajemenCustomHasil",
            },
            // {
            //     name: "Custom HTML Footer",
            //     route: "manajemenCustomJS",
            // }

        ]
    },
    {
        name: "Dukun Togel",
        // route: "manajemenPageList"
        submenu: [
            {
                name: "Periode",
                route: "periodeDukunTogel",
            },
            {
                name: "Hasil",
                route: "hasilDukunTogel",
            },

            {
                name: "Custom HTML Footer",
                route: "customDukunTogel",
            }

        ]
    },
]





export default adminmenu
