const twoD = [
  {
    angka: "01 = 05-95-12-45",
    text: "Setan - Bandeng - Obor - JambuMente - Tangan - BetaraKala"
  },
  {
    angka: "02 = 16-53-09-35",
    text: "Sarjana - Bekicot - LoncatTinggi - Wortel - Sandal - BetaraBrahma"
  },
  {
    angka: "03 = 32-53-85-25",
    text: "OrangMati - Angsa - LoncatGalah - Sawi - Kaki - Subali"
  },
  {
    angka: "04 = 12-65-05-15",
    text: "Kwan in - Merak - LompatJauh - Kangkung - Balon - DewiRatih"
  },
  {
    angka: "05 = 01-89-10-39",
    text: "KepalaRampok - Singa - LoncatIndah - KayuManis - KeretaApi - GuruLangit"
  },
  {
    angka: "06 = 20-91-51-41",
    text: "DewiBulan - Kelinci - Renang - Kapas - Boneka - DewiSri"
  },
  {
    angka: "07 = 24-58-57-08",
    text: "Pelayan - Babi - PerahuLayar - Bawang - Pancing - Sulatri"
  },
  {
    angka: "08 = 17-57-04-07",
    text: "MalingKecil - Macan - MotorBoat - Kecubung - Pasar - TalaMaria"
  },
  {
    angka: "09 = 33-87-88-37",
    text: "Jendral - Kerbau - Mendayung - Pepaya - Jala - Bima"
  },
  {
    angka: "10 = 18-82-03-32",
    text: "Kelenteng - Kelabang - Menyelam - Kelapa - Bir - SangPamuji"
  },
  {
    angka: "11 = 15-77-02-27",
    text: "MenteriSerakah - Anjing - LariCepat - Sapu - Kipas - Sengkuni"
  },
  {
    angka: "12 = 04-69-17-19",
    text: "PenasehatPerang - Kuda - LariGawang - Lemon - BolaLampu - Wibisana"
  },
  {
    angka: "13 = 14-79-07-29",
    text: "PenjagaPintu - Gajah - LariEstafed - KipasAngin - Keris - PrabuKesa"
  },
  {
    angka: "14 = 13-96-08-46",
    text: "PotongBabi - Onta - TolakPeluru - Jembatan - Spet(SUNTIKAN) - JayaLangsuan"
  },
  {
    angka: "15 = 11-54-00-04",
    text: "Hakim - Tikus - LemparMartil - KantorPos - Skrup - Kresna"
  },
  {
    angka: "16 = 02-74-15-24",
    text: "OrangSakitGUDIK - Tawon - LemparCakram - Surat - Nanas - Jembawan"
  },
  {
    angka: "17 = 08-88-13-38",
    text: "Pemadat - Bangau - LemparLembing - Durian - Telepon - ButoTerong"
  },
  {
    angka: "18 = 10-78-01-28",
    text: "KasUang - Kucing - PoloAir - Lombok - KantorPolisi - Bisma"
  },
  {
    angka: "19 = 27-62-54-12",
    text: "PelacurKelasTINGGI - KupuKupu - SepakBola - srikaya(sirsat) - BanSepedah - Banowati"
  },
  {
    angka: "20 = 06-72-19-22",
    text: "ISTRIsejati - Lalat - VollyBal - Palu - BanMobil - Setiawati"
  },
  {
    angka: "21 = 22-93-55-43",
    text: "PelacurUMUM - Walet - BuluTANGKIS - Permen - Kapak - Lesmanawati"
  },
  {
    angka: "22 = 21-70-50-20",
    text: "PetiMati - Capung - Tenis - Terong - Harmonika - Arjuna&amp;Sembadra"
  },
  {
    angka: "23 = 30-84-81-34",
    text: "SetanGantung - Kera - BolaBasket - Pisang - Piano - Wilkampana"
  },
  {
    angka: "24 = 07-66-14-16",
    text: "SumberAir - Katak - TenisMeja - SikatGIGI - Padi - DewaRUCI"
  },
  {
    angka: "25 = 35-85-82-03",
    text: "MenantuRaja - Rajawali - Baseball - Jagung - TapalGIGI - KangsaDEWA"
  },
  {
    angka: "26 = 31-90-80-40",
    text: "Raja - Naga - Hockey - Ganggang - MesinJAHIT - SamiAji"
  },
  {
    angka: "27 = 19-61-06-11",
    text: "WanitaCantik - KuraKURA - BolaSODOK - SabunBUBUK - Otak - DewiSupraba"
  },
  {
    angka: "28 = 29-68-56-18",
    text: "PencariKAYU - Ayam - Menembak - Tomat - Jarum - Nakula"
  },
  {
    angka: "29 = 28-63-53-13",
    text: "PendetaSAKTI - Belut - Panahan - Kursi - Koran - Sidiwacana"
  },
  {
    angka: "30 = 23-99-58-49",
    text: "Nelayan - IkanMAS - AngkatBESI - Belimbing - WC - NagaTATMALA"
  },
  {
    angka: "31 = 26-94-59-44",
    text: "Kelamin - Udang - Senam - CacingPita - SangkarBurung - YuyuRumpung"
  },
  {
    angka: "32 = 03-60-18-10",
    text: "AhliNUJUM - Ular - Yudo - KamarMandi - Tali - AbiYASA"
  },
  {
    angka: "33 = 09-86-16-36",
    text: "Pengemis - LabaLABA - Gulat - Gigi - Sabun - Petruk"
  },
  {
    angka: "34 = 36-73-89-23",
    text: "OrangBUTA - Rusa - Silat - Jamu - ParuPARU - Destarata"
  },
  {
    angka: "35 = 25-75-52-02",
    text: "Wanita - KAMBING - Tinju - Lambung - JalanJALAN - Drupadi"
  },
  {
    angka: "36 = 34-83-87-33",
    text: "PendetaWANITA - Musang - BalapSEPEDAH - MANGGIS - RumahObat - SayemPraba"
  },
  {
    angka: "37 = 38-59-83-09",
    text: "OrangBONGKOK - IkanGAbus - BalapMOBIL - Anggur - BH - Truk - Gareng"
  },
  {
    angka: "38 = 37-67-84-17",
    text: "PutriRAJA - Cendrawasih - BalapSEPEDAHmotor - Engsel - Drum - Untari"
  },
  {
    angka: "39 = 44-55-77-05",
    text: "Kekasih - Kalajengking - BalapKUDA - Topi - Bemo - Narasuma"
  },
  {
    angka: "40 = 43-76-78-26",
    text: "Penolong - Glatik - Golf - Tang - Peci - Widura"
  },
  {
    angka: "41 = 49-56-76-06",
    text: "Pahlawan - Kepiting - LompatKUDA - Lilin - Sabuk - Warsaya"
  },
  {
    angka: "42 = 45-97-72-47",
    text: "JejakaTUA - Buaya - GerakJALAN - Catur - Dokter - LesmanaWIDAKTA"
  },
  {
    angka: "43 = 40-71-41-21",
    text: "JANDAmuda(Rondho) - IkanSURO - Anggar - Mawar - Grendel - SumbaDRA"
  },
  {
    angka: "44 = 39-81-86-31",
    text: "Berandal - Badak - SkiAIR - Seruling - Sisir - Citraksa"
  },
  {
    angka: "45 = 42-51-75-01",
    text: "Pengembara - Banteng - TerbangLAyang - Kendi - Tas - Rama"
  },
  {
    angka: "46 = 48-64-73-14",
    text: "NenekMOYANG - OrangUTAN - TerjunBEBAS - Sikat - Toko - HyangWENANG"
  },
  {
    angka: "47 = 50-92-21-42",
    text: "Banci - Zebra - UpacaraBENDERA - Tangga - Hotel - Stuna"
  },
  {
    angka: "48 = 46-00-79-50",
    text: "SiCEROBOH - Landak - MainCATUR - Garuk - GedungBIOSKOP - Dasamuka"
  },
  {
    angka: "49 = 41-80-70-30",
    text: "Drakula - Kelelawar - MendakiGUNUNG - Cetok - Rok - BetariDURGA"
  },
  {
    angka: "50 = 47-98-74-48",
    text: "OrangESKIMO - Beruang - PembawaOBOR - Pacul - Guru - Bagong"
  },
  {
    angka: "51 = 55-45-22-95",
    text: "AhliFILSAFAT - Kerang - PateLELE - Tebu - Celana - Narodo"
  },
  {
    angka: "52 = 66-03-99-85",
    text: "RajaLAUT - IkanPAUS - MainTali - Matahari - Dompet - Antasena"
  },
  {
    angka: "53 = 82-02-35-52",
    text: "PenjualSILAT - IkanDURI - Akrobat - Rambutan - Taxi - AbiMANYU"
  },
  {
    angka: "54 = 62-15-95-65",
    text: "RAJAkera - IkanLELE - SEPATUroda - Kalung - Dokar - KeraHANOMAN"
  },
  {
    angka: "55 = 51-39-20-89",
    text: "Pertapa - Kanguru - Kasti - Gelang - Kemaron - RDseta"
  },
  {
    angka: "56 = 70-41-71-91",
    text: "Budak - IkanDUYUNG - BERINGEN - Kenanga - Cikar - Limbuk"
  },
  {
    angka: "57 = 74-08-47-58",
    text: "AnakSAKTI - UlatSUTRA - LAYANGlayang - Sepatu - Ranjang - GatotKACA"
  },
  {
    angka: "58 = 67-07-94-57",
    text: "Penari - CumiCUMI - MainKELERENG - RUMAH - Sekolahan - Selir"
  },
  {
    angka: "59 = 83-37-38-87",
    text: "PutraRAJA - KakakTUA - DAKON - Kedondong - Kaos - RD lesmana"
  },
  {
    angka: "60 = 68-32-93-82",
    text: "KepalaPOLISI - Cecak - Karambol - Delima - Handuk - Sentiyaki"
  },
  {
    angka: "61 = 65-27-92-77",
    text: "Pedagang - Kecoak - Gendongan - KacaMATA - BUKU - BalaDEWA"
  },
  {
    angka: "62 = 54-19-27-69",
    text: "Pagoda - WalangKADUNG - Petan - Termos - SelendangPELANGI - CANDIsaptaARGA"
  },
  {
    angka: "63 = 64-29-97-79",
    text: "PendekarWANITA - Kumbang - trekSANDO - Bantal - Jendela - Larasati"
  },
  {
    angka: "64 = 63-46-98-96",
    text: "DewaUANG - KudaLAUT - Bandulan - Apel - Guling - BetaraINDRA"
  },
  {
    angka: "65 = 61-04-90-54",
    text: "RajaSETAN - IKANhiu - Kayang - Klompen - Petromak - KalaSRINGGI"
  },
  {
    angka: "66 = 52-24-25-74",
    text: "DewaBUMI - Jerapah - Sawatan - Sukun - Gelas - AntaBOGA"
  },
  {
    angka: "67 = 58-38-23-88",
    text: "PenjualDAGING - BurungONTA - Egrang - Sendok - KorekAPI - ABILAWA"
  },
  {
    angka: "68 = 60-28-91-78",
    text: "PembuatPEDANG - BurungHANTU - PanjatPINANG - Pisau - Garbu - Cepot"
  },
  {
    angka: "69 = 77-12-44-62",
    text: "PencariJEJAK - MIMI - Engklek - Gunting - Gunung - AntaREJA"
  },
  {
    angka: "70 = 56-22-29-72",
    text: "Palingma - Keledai - TarikTAMBANG - LampuMINYAK - RumahMAKAN - AdipatiKARNA"
  },
  {
    angka: "71 = 72-43-45-93",
    text: "Pemburu - MacanTUTUL - LemparKARET - Sumur - Baju - Pandu"
  },
  {
    angka: "72 = 71-20-40-70",
    text: "DewaLANGIT - IkanTERBANG - Okol - KranAIR - ARLOJI - BetaraGuru"
  },
  {
    angka: "73 = 80-34-31-84",
    text: "TuanTANAH - Semut - Tulupan - AntingAnting - Bintang - Dursasana"
  },
  {
    angka: "74 = 57-16-24-66",
    text: "BajakLAUT - Pinguin - Setipan - Gentong - Radio - Indrajit"
  },
  {
    angka: "75 = 85-35-32-53",
    text: "SuamiISTRI - Bebek - BalapanLARI - Nangka - Lemari - Ratih&amp;Kamajaya"
  },
  {
    angka: "76 = 81-40-30-90",
    text: "JendralWANITA - Nyamuk - Teplekan - Mata - Timbangan - Srikandi"
  },
  {
    angka: "77 = 69-11-96-61",
    text: "Walikota - Penyu - Bengkel - Cincin - Payung - Togog"
  },
  {
    angka: "78 = 79-18-46-68",
    text: "OrangKAYA - IkanGERGAJI - BalapBECAK - Semangka - Wajan - Lesmana MandraKumala"
  },
  {
    angka: "79 = 78-13-43-63",
    text: "JendralSERAKAH - OrongORONG - Okol - JerukBALI - kompor - suyudana"
  },
  {
    angka: "80 = 73-49-48-99",
    text: "KepalaDESA - Bajing - Apolo - potlot - Ceret - Semar"
  },
  {
    angka: "81 = 76-44-49-94",
    text: "Penipu - Kancil - Damdaman - Hidung - Cangkir - AswaTAMA"
  },
  {
    angka: "82 = 53-10-28-60",
    text: "Gembala - KudaNIL - AS - Telinga - Berlian - Udawa"
  },
  {
    angka: "83 = 59-36-26-86",
    text: "IbuSURI - IkanLAYUR - DADU - Kumis - Pipa - DewiKUNTI"
  },
  {
    angka: "84 = 86-23-39-73",
    text: "Budha - Kalkun - Salto - Mulut - KacangTANAH - Bagaspati"
  },
  {
    angka: "85 = 75-25-42-52",
    text: "WanitaSIHIR - Jangkrik - LatihanHANSIP - Teratai - Pintu - SarpaKENAKA"
  },
  {
    angka: "86 = 86-33-37-83",
    text: "DewaMAUT - IkanSAMPAN - GerakBADAN - Salak - Rokok - Yamadipati"
  },
  {
    angka: "87 = 88-09-33-59",
    text: "OrangGILA - Betet - KerjaBAKTI - Botol - Toilet - Buriswara"
  },
  {
    angka: "88 = 87-17-34-67",
    text: "DewiMEGA - DOMBA - BalapKARUNG - JERUKmanis - piring - Wilutama"
  },
  {
    angka: "89 = 94-05-67-55",
    text: "Pemabuk - IkanBENDERA - Setopan - JerukKEPROK - EMBER - Bomanarakasura"
  },
  {
    angka: "90 = 93-26-68-76",
    text: "Tawanan - Trenggiling - PerempatanJALAN - PIL - Sawah - Shinta"
  },
  {
    angka: "91 = 99-06-66-56",
    text: "SilumanAIR - SERIGALA - Ambulan - Bambu - Toples - Witaksini"
  },
  {
    angka: "92 = 95-47-62-97",
    text: "PutriKIPASbesi - IkanTENGIRI - GarisFINISH - Apokat - Sarung - SitiSUNDARI"
  },
  {
    angka: "93 = 90-21-61-71",
    text: "Penjilat - BabiHUTAN - PerahuLAYAR - KaosKAKI - LAPANGAN - Durna"
  },
  {
    angka: "94 = 89-31-36-81",
    text: "KwanKONG - IkanKAKAP - Pemandian - Jambu - Pen - P.Salya"
  },
  {
    angka: "95 = 92-01-65-51",
    text: "Petani - Perkutut - JalanRAYA - Kunci - PisauCUKUR - Irawan"
  },
  {
    angka: "96 = 98-14-63-64",
    text: "Prajurit - IkanNUS - Laut - Mangga - MinyakANGIN - CITRAYUDA"
  },
  {
    angka: "97 = 00-42-11-92",
    text: "RAKSASA - TOKEK - KaliBRANTAS - SIRSAT - LemariES - PRAHASTA"
  },
  {
    angka: "98 = 96-50-69-00",
    text: "PenjagaMALAM - Tongkol - TV - Lengkeng - Kecelakaan - Trijati"
  },
  {
    angka: "99 = 91-30-60-80",
    text: "HidungBELANG - BurungJALAK - BAYI - Kodak - Meja - Arjuna"
  },
  {
    angka: "00 = 97-48-64-98",
    text: "Penyair - Tapir - Sempritan - Rembulan - Tanggalan - KumboKARN0"
  }
]


export default twoD;
