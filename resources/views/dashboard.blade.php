@extends('layouts.app')
@section('content')
<Navbar level="{!! Auth::user()->level !!}"></Navbar>
<main class="py-4">
  <div class="container">
    {{-- <div class="row justify-content-center"> --}}
    <div class="col-md-12">
      <router-view></router-view>
    </div>
    {{-- </div> --}}
  </div>


</main>


@endsection
