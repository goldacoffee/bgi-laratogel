@extends('layouts.app')


@section('content')
@includeIf('keluaran.mobileOverlay')

<div class="center-container">
    <div class="main-container">
        <div class="nav">
            <div>
                <a href="/"><img alt="{{ option('websiteName') }} logo" src="{{ option('websiteLogo') }}"
                        style="padding:10px" /></a>
            </div>
            <div class="navbar" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
                <ul class="top-nav">

                    @includeIf('menu.pc')

                    {{-- {!! buildMenu() !!}
            @includeIf('menu.pc') --}}

                </ul>
                <img alt="close menu" src="{{ asset('/images/menu-nav.svg') }}"
                    class="mobile-menu-icon open-mobilenav" />
            </div>


        </div>


        @includeIf('partial.headerbanner')



        <div class="results-container">
            <h1 class="center">{{ option('livedrawPageTitle') }}</h1>
            <br />
            <div class="results-grid">


                @foreach ($data as $key => $item)




                <!-- RESULT ITEM -->
                <div class="results-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                    <a href="{{ route('gotoPage',option('livedrawPermalinkPrefix').'-'.strtolower($key) ) }}">
                        <div class="results-country" itemprop="name">
                            <img class="lazy" width="60" height="40"
                                src="/images/countryflags/{{ strtolower($key) }}.svg" />
                            <span>
                                <span class="none">Keluaran Togel</span>
                                {{ $item->name }}
                                <span class="none">Hari Ini</span>
                                <span class="none">tanggal</span>
                            </span>
                        </div>
                        {{-- <div class="results-meta">
                                <span>{{ $item->datakeluaran[0]->hari }},
                        {{ formatDate($item->datakeluaran[0]->idTanggal) }}</span>
                        <span>{{ $item->kode }}-{{ $item->datakeluaran[0]->periode }}</span>
                </div> --}}
                <div class="results-result lazy"
                    style="background-image:url('{{ ( isset($item->gambar) && $item->gambar !== null) ? $item->gambar : '/images/countryflags/'.strtolower($key).'.svg' }}');"
                    itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span
                        itemprop="text">LIVEDRAW</span>
                </div>
                </a>
                <div class="results-history">
                    <div class="pasaran">
                        <span style="margin:auto;">Jam Result :
                            {{ (isset($item->jam_buka) && $item->jam_buka !==null) ? $item->jam_buka : '00:00' }}</span>
                    </div>
                    {{-- <div class="title">{{ option('homepageKeluaranLamaText') }}
                </div> --}}
                <div style="width: 100%; height: 2px"><img src="/images/border-sep.png" style="width: 100%"
                        height="2" />
                </div>


                {{-- <div>hari,tanggal<span>keluaran</span>
                    </div> --}}


                <div class="more"><a class="button buttonKeluaranLama"
                        href="{{ route('gotoPage',option('livedrawPermalinkPrefix').'-'.strtolower($key) ) }}">{{ option('livedrawTextButton') }}</a>
                </div>
            </div>
        </div>



        @endforeach






    </div>
</div>

<br />

{{-- @includeIf('partial.share') --}}
<div class="share-bar" style="margin:0px;">
    <h3>{{ option('homepageShareText') }}</h3>
    <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}" target="_blank">
        <img src="/images/social/facebook-logo.svg" width="20" />
    </a>
    <a href="https://twitter.com/intent/tweet?text=url={{ url()->full() }}" target="_blank">
        <img src="/images/social/twitter-letter-logo.svg" width="20" />
    </a>
    @if (option('whatsappNumber') == '')
    <a><img src="/images/social/whatsapp-logo.svg" width="20" />
    </a>
    @else
    <a href="https://wa.me/{{ option('whatsappNumber') }}" target="_blank"><img src="/images/social/whatsapp-logo.svg"
            width="20" />
    </a>
    @endif

    <a href="https://telegram.me/share/url?url={{ url()->full() }}" target="_blank"><img
            src="/images/social/telegram_logo_white.svg" width="20" />
    </a>
</div>



<br />
{{-- <div style="width: 100%; height: 2px"><img src="/images/border-sep.png" style="width: 100%" height="2" />
        </div> --}}
<br />
<br />



</div>
</div>


@includeIf('partial.bottomnav')
@includeIf('partial.customfooter')




<div class="footer-container">


</div>

@includeif('partial.footerbanner')
@includeIf('keluaran.footerModal')

@endsection
