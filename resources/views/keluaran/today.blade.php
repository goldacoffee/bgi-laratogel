@extends('layouts.app')
@section('content')
@includeIf('keluaran.mobileOverlay')
<div class="center-container">
  <div class="main-container">
    <div class="nav">
      <div>
        <a href="/"><img alt="{{ option('websiteName') }} logo" src="{{ option('websiteLogo') }}"
            style="padding:10px" /></a>
      </div>
      <div class="navbar">
        <ul class="top-nav">

          {{-- {!! buildMenu() !!} --}}
          @includeIf('menu.pc')

        </ul>
        <img alt="close menu" src="{{ asset('/images/menu-nav.svg') }}" class="mobile-menu-icon open-mobilenav" />
      </div>


    </div>
    @includeIf('partial.headerbanner')
    <div class="article-container">
      <article>

        {{-- <img itemprop="image" src="/images/prediksi_bg/prediction_bg_hongkong.jpg" /> --}}

        <div class="content">
          <div class="country-results">
            <div class="country-wrapper">

              <div class="country-name">
                <img src="{{ asset('/images/countryflags/'.strtolower($data->_info->kode). '.svg')  }}"
                  alt="Singapore flag" />
                <span>{{ ucwords($provider) }}</span>
              </div>

              <div class="country-result-today">
                <h1 class="top-title">{{ sprintf(option('titlePageKeluaranPerProvider'),ucwords($provider)) }}</h1>
                {{-- <h3><span>{{ sprintf(option('titlePageKeluaranPerProvider'),ucwords($provider)) }}</span></h3> --}}
              </div>








              <div class="country-result-hair-ini">
                <h1>{{ $data->_today->keluaran }}</h1>
                <h5>{{ $data->_today->hari }}, {{ $data->_today->idTanggal }}</h5>


                <a class="button inline buttonTomorrow"
                  href="{{ permalinkTomorrow($provider,$data->_tomorrow->hari,$data->_tomorrow->tanggal) }}"
                  onclick="gtag('event', 'next_prediksi', {country: 'Hongkong'});">{{ sprintf(option('tomorrowButtonPermalinkText'),ucwords($provider)) }}</a>

              </div>

              <a class="button inline button-livedraw"
                href="{{ route('gotoPage',option('livedrawPermalinkPrefix').'-'. strtolower($data->_info->kode) ) }}">
                LIVE DRAW {{ strtoupper($provider) }} </a>
                <br>

              <div class="top3bandar">
                @includeIf('partial.top3bandar')
              </div>
              <div class="bandartogel-button-area">
                <a href="{{ route('gotoPage',loadPageSlug(option('idPageBandarTogel'))) }}" class="button-bandartogel"
                  rel="noopener" target="_blank">{{ option('textTombolBandarTogel') }}</a>

              </div>
              <div class="statistik-banner">
                {!! loadBanner('statistik') !!}
              </div>

            </div>
          </div>

          <!-- START POLA2D -->
          <br>
          <h2 class="single-title">Pola 2D {{ $provider }} [10 Undian]</h2>

          {{-- <table align="center" class="table-prediksi-askop table-bordered responsive-table" border="1"
            style="width:100%;height:100%;"> --}}
          <table class="shio-table">

            @foreach ($data->_pola2D as $key => $item)
            <tr>
              <td class="head">{{ ucwords($key) }}</td>
              @foreach ($item as $item)
              <td class="{{ strtolower($item) }}">{{ $item }}</td>

              @endforeach


            </tr>
            @endforeach

          </table>
          <br>
          <br>
          <!-- END POLA2D -->



          <h2 class="single-title">Statistik Keluaran {{ $provider }} [30 Undian]</h2>

          <div class="frequency-cointainer">
            @foreach ($data->_askor as $key => $value)
            <div class="frequency-card">
              <div class="position">{{ strtoupper($key) }} </div>
              <div class="frequency-graph">

                @foreach ($value->data as $index => $valuex)
                <div class="frequency-bar">
                  <div class="bar" id="bar-0-0" div="" style="height: {{ ($valuex / 30) * 300 }}%;">
                    <div>{{ ($valuex != 0 ) ? $valuex : '' }}x</div>
                  </div>
                  <div class="number"> {{ $index }} </div>
                </div>
                @endforeach

              </div>

              <br>
              {{-- {{ print_r($value) }} --}}
              <table class="prediksi-table">
                <tbody>
                  <tr>
                    <th colspan="2">Mayoritas Angka</th>
                  </tr>
                  <tr>
                    <td>{{ $value->kiri }}</td>
                    <td>{{ $value->kanan }}</td>

                  </tr>
                </tbody>
              </table>
            </div>

            @endforeach

          </div>




          <!-- SHIO -->
          <br>
          <br>
          {{-- <center><a class="button inline yellow" href="http://165.22.253.162/cara-main-togel-pakai-angka-shio">Cara
              Main Togel Pakai Angka Shio</a></center> --}}
          <h2 class="single-title">Statistik Shio {{ $provider }} [30 Undian]</h2>

          <div class="bukushio-button-area">
            <a href="{{ route('gotoPage',loadPageSlug(option('idPageBukuShio'))) }}" class="button-bukushio"
              rel="noopener" target="_blank">{{ option('textTombolBukuShio') }}</a>

          </div>



          <div class="container">
            <table class="shio-table">
              @foreach ($data->_shioReport as $item)
              <tr>
                <td class="head"><b>{{ $item->shio }}</b></td>
                <td class="data-shio">
                  <div class="bar" style="width:{{ ($item->jlh/30) *300 }}%;"><b>x{{ $item->jlh }}</b></div>&nbsp;
                </td>
              </tr>
              @endforeach
            </table>
          </div>

          <!-- END SHIO -->
          <!-- START COLOK BEBAS -->
          <br />
          <br />
          <h2 class="single-title">Statistik Colok Bebas {{ $provider }} [30 Undian]</h2>
          <div class="container">
            <table class="shio-table">
              @php
              $total = array_sum((array)$data->_colokBebasReport->total)
              @endphp
              @foreach ($data->_colokBebasReport->data[0] as $key => $item)
              <tr>
                <td class="head"><b>{{ $key }}</b></td>
                <td class="data-shio">
                  <div class="bar" style="width:{{ ($item/$total) *300 }}%;"><b>x{{ $item }}</b></div>&nbsp;
                </td>
              </tr>
              @endforeach
            </table>
          </div>
          <br>
          <!-- END COLOK BEBAS -->

          {{-- <!-- START KOMBINASI -->
          <br />
          <h2 class="single-title">Statistik Kombinasi {{ $provider }} [30 Undian]</h2>
          <div class="container">
            <table class="shio-table">
              @php

              @endphp
              @foreach ($data->_kombinasi as $key => $item)
              <tr>
                <td class="head"><b>{{ $item->status }}</b></td>
                <td class="data-shio">
                  <div class="bar" style="width:{{ (($item->jumlah/30) * 300 )}}%;"><b>x{{ $item->jumlah }}</b>
                  </div>&nbsp;
                </td>
              </tr>
              @endforeach
            </table>
          </div>
          <br>
          <!-- END KOMBINASI --> --}}



          <!-- START UMUM -->
          <br />
          <h2 class="single-title">Statistik 50-50 Umum {{ $provider }} [30 Undian]</h2>
          <div class="container">
            <table class="shio-table">
              @php

              @endphp
              @foreach ($data->_umum as $key => $item)
              <tr>
                <td class="head"><b>{{ $item->status }}</b></td>
                <td class="data-shio">
                  <div class="bar" style="width:{{ ($item->jumlah/30) * 100}}%;"><b>x{{ $item->jumlah }}</b>
                  </div>&nbsp;
                </td>
              </tr>
              @endforeach
            </table>
          </div>
          <br>
          <!-- END UMUM -->


          <!-- START DASAR -->


          <br />
          <h2 class="single-title">Statistik Dasar {{ $provider }} [30 Undian]</h2>
          <div class="container">
            <table class="shio-table">
              @php

              @endphp
              @foreach ($data->_dasar as $key => $item)
              <tr>
                <td class="head"><b>{{ $key }}</b></td>
                <td class="data-shio">
                  <div class="bar " style="width:{{ ($item/30) * 100}}%;"><b>x{{ $item }}</b>
                  </div>&nbsp;
                </td>
              </tr>
              @endforeach
            </table>
          </div>
          <br>





          <!-- START HISTORY-->
          <br>
          <h2 class="single-title">History Keluaran {{ $provider }} Sebelumnya</h2>
          <div class="row header">
            <div>Date</div>
            <div>Draw #</div>
            <div>Result</div>
          </div>
          @foreach ($data->_last30DaysReport as $item)
          <div class="row">
            <div class="result-date">{{ $item->hari }}, {{ formatDate($item->idTanggal) }}
            </div>
            <div>{{ $data->_info->kode }} - {{ $item->periode }}</div>
            <div>{{ $item->keluaran }}</div>
          </div>
          @endforeach

          @includeIf('partial.share')


          <!-- END HISTORY-->


          @if ($faqKeluaran)
          <article class="mb-3">
            {!! $faqKeluaran->html !!}
          </article>
          @endif

          @if ($aboutKeluaran)
          <article class="mb-3">
            {!! $aboutKeluaran->html !!}
          </article>
          @endif


        </div>
      </article>
      @includeIf('partial.sidebar')
    </div>

    <br />
    {{-- <img src="{{ asset('/images/border-sep.png') }}" alt="border separator" width="100%" height="2" /> --}}


  </div>
</div>
@includeIf('partial.bottomnav')
@includeIf('partial.customfooter')
<div class="footer-container">

</div>

<div id="lucky-modal" class="how-lucky-modal">
  <a href="" class="close-lucky toggle-modal-lucky"><img alt="close nav"
      src="{{ asset('/images/close-nav2.svg') }}" /></a>
  <div class="howlucky-container">
    <div class="left"></div>
    <div class="right">

      <h1>Seberapa Beruntung Anda?</h1>
      <h4>Cek nomor anda berdasarkan ratusan keluaran.</h4>
      <form id="lucky-form">
        <label for="lucky-input">Angka 4D</label>
        <input pattern="[0-9]*" inputmode="numeric" type="number" id="lucky-input" maxlength="4" placeholder="1536"
          class="howluckyinput" />
        <br />
        <input type="submit" value="Cek!" class="howluckysubmit" />
      </form>
    </div>
  </div>
</div>

@includeif('partial.footerbanner')
@includeIf('keluaran.footerModal')

<script type="application/ld+json">
  {!! jsonLD_breadcumb('content',$provider) !!}
</script>
<script type="application/ld+json">
  {!! jsonLD_home() !!}
</script>

@endsection
