<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($keluaran as$item)

    <url>
        <loc>{{  route('datakeluaran',strtolower($item->nama)) }}</loc>
        <lastmod>{{ date('Y-m-d') }}</lastmod>
    </url>

    @endforeach
    @foreach ($faq as $item)

    <url>
        <loc>{{route('gotoPage',$item->slug)}}</loc>
        <lastmod>{{ date('Y-m-d') }}</lastmod>
    </url>

    @endforeach
    @foreach ($livedraw as $item)

    <url>
        <loc>{{route('gotoPage',$item->slug)}}</loc>
        <lastmod>{{ date('Y-m-d') }}</lastmod>
    </url>

    @endforeach
    @foreach ($customsitemap as $item)

    <url>
        <loc>{{route('gotoPage',$item)}}</loc>
        <lastmod>{{ date('Y-m-d') }}</lastmod>
    </url>

    @endforeach


</urlset>
