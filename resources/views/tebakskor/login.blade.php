<div class="login">
  @if($errors->any())
  <div class="error-login">
    Email / Password yang anda masukkan salah, silahkan coba lagi
  </div>
  @endif
  <form action="{{ route('login') }}" method="post">
    @csrf
    <h3 class="user-please-login"> Silakan Login Untuk Mengirim Prediksi</h3>
    <input class="field" type="text" placeholder="Username" id="email" name="email">
    <input class="field" type="password" placeholder="Password" name="password" id="password">
    <br>
    <input class="btn" type="submit" value="LOGIN">

  </form>
</div>
