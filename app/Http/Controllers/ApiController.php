<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Menu;
use App\Options;
use App\Pages;
use App\Sidebar;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cache;
use function GuzzleHttp\Psr7\try_fopen;

use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function list()
    {
        $data = Options::where('name', '<>', 'custom_css')
            ->where('name', '<>', 'custom_js')
            ->where('name', '<>', 'custom_menu')
            ->orderBy('name', 'ASC')->get();
        return $data;
    }
    public function update(Request $request)
    {
        try {
            $data = Options::updateOrCreate([
                'name' => $request->name
            ], [
                'value' => $request->value
            ]);

            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'data' => null
            ], 201);
        }
    }

    // sidebar
    public function listSidebar()
    {
        try {

            $data =  Sidebar::orderBy('position')->get();

            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()
            ], 201);
        }
    }

    public function detailSidebar($id)
    {
        try {
            $data =  Sidebar::where('id', $id)->first();
            return response([
                'success' => true,
                'data' => $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }
    public function deleteSidebar(Request $request)
    {
        try {
            $dataUser =  Sidebar::where('id', $request->id)->delete();
            return response([
                'success' => true,

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }
    public function addSidebar(Request $request)
    {
        try {

            $data =   Sidebar::updateOrCreate([
                'id' => $request->id
            ], [
                'name' => $request->name,
                'title' => $request->title,
                'html' => $request->html

            ]);

            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()
            ], 201);
        }
    }



    // users

    public function listUser()
    {
        $data  = User::where('level', 'user')->get();
        return response([
            'success' => true,
            'data' => $data
        ], 200);
    }

    public function userDetails(Request $request)
    {
        try {
            $dataUser =  User::find($request->id);
            return response([
                'success' => true,
                'data' => $dataUser
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'data' => null
            ], 201);
        }
    }
    public function userDelete(Request $request)
    {
        try {
            $dataUser =  User::where('id', $request->id)->delete();
            return response([
                'success' => true,

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,

            ], 201);
        }
    }


    // menu
    public function listMenu()
    {
        try {

            $menu = Menu::orderBy('position', 'ASC')->get();
            return response([
                'success' => true,
                'data' => $menu
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'data' => null
            ], 201);
        }
    }


    public function updateMenu(Request $request)
    {

        try {
            $x = 1;

            foreach ($request->data as $req) {
                // return $req;
                Menu::where('id', $req['id'])
                    ->update(['position' => $x]);
                $x++;
            }

            return response([
                'success' => true,
                'msg' => 'Menu position updated'
            ]);
        } catch (\Throwable $th) {

            return response([
                'success' => false,
                'msg' => $th->getMessage()
            ]);
        }
    }

    public function allowedProvider()
    {
        $array = allProvider();

        return response([
            'success' => true,
            'data' => (array) $array
        ], 200);
    }
    public function addMenu(Request $request)
    {
        try {
            $lastPosition =  Menu::orderBy('position', 'DESC')->first();
            if ($lastPosition === null) {
                $lastPosition = 0;
            } else {
                $lastPosition = $lastPosition->position;
            }

            Menu::updateOrCreate(
                [
                    'id' => $request->id
                ],
                [

                    'name' => $request->name,
                    'type' => $request->type,
                    'value' => $request->value,
                    'position' => $lastPosition + 1
                ]
            );

            return response([
                'success' => true,
                'msg' => "Success Add Menu"

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => true,
                'msg' => $th->getMessage()

            ], 200);
        }
    }

    public function deleteMenu(Request $request)
    {

        try {
            $dataUser =  Menu::where('id', $request->id)->delete();
            return response([
                'success' => true,
                'msg' => 'success delete menu'

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }

    public function detailsMenu($id)
    {
        try {
            $data =  Menu::where('id', $id)->first();
            return response([
                'success' => true,
                'data' => $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }


    // page

    public function deletePage(Request $request)
    {
        try {
            $dataUser =  Pages::where('id', $request->id)->delete();
            return response([
                'success' => true,
                'msg' => 'success delete halaman'

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }
    public function addPage(Request $request)
    {
        // return $request;

        try {
            $data =  Pages::updateOrCreate(
                [
                    'id' => $request->id
                ],
                [
                    // Str::slug($pages->title, '-');
                    'slug' => ($request->slug == '' ?  Str::slug($request->title, '-') : $request->slug),
                    'title' => $request->title,
                    'description' => $request->description,
                    'featured_image' => $request->featured_image,
                    'html' => ($request->html == null || $request->html === '' ? '<!-- ISI KODE DIBAWAH. -->' :  $request->html),
                    'tags' => $request->tags,
                ]
            );
            if ($request->slug === 'pengumuman-dukun') {
                Cache::forget('customCloseMessageDukun');
            }
            return response([
                'success' => true,
                'data' => $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }

    public function detailPage($id)
    {
        try {
            $data =  Pages::find($id);
            return response([
                'success' => true,
                'data' => $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }

    public function pageForMenu()
    {
        try {
            $data =  Pages::select('id', 'slug', 'title')->get();
            return response([
                'success' => true,
                'data' =>   $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }
    public function listPage()
    {


        try {
            $data =  Pages::all();
            return response([
                'success' => true,
                'data' =>  $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }

    public function customMENU()
    {
        try {
            $data =  option('custom_menu');
            return response([
                'success' => true,
                'data' => $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }
    public function customMENUUpdate(Request $request)
    {

        try {

            $data = Options::where('name', 'custom_menu')
                ->update(['value' => $request->data]);

            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'data' => $th->getMessage()
            ], 201);
        }
    }



    // custom
    public function customCSS()
    {
        try {
            $data =  option('custom_css');
            return response([
                'success' => true,
                'data' => $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }
    public function customCSSUpdate(Request $request)
    {

        try {

            $data = Options::where('name', 'custom_css')
                ->update(['value' => $request->data]);



            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'data' => $th->getMessage()
            ], 201);
        }
    }

    public function customJS()
    {
        try {
            $data =  option('custom_js');
            return response([
                'success' => true,
                'data' => $data

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'msg' => $th->getMessage()

            ], 201);
        }
    }
    public function customJSUpdate(Request $request)
    {

        try {

            $data = Options::where('name', 'custom_js')
                ->update(['value' => $request->data]);



            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'data' => $th->getMessage()
            ], 201);
        }
    }

    public function bannerAdd(Request $request)
    {
        // return $request;
        try {



            $banner = $request->id !== 'new' ? Banner::findOrFail($request->id) : new Banner(['id' => Str::uuid()]);
            // return $banner;
            $data = [
                'name' => 'Banner ' . $request->location,
                'location' => $request->location,
                'html' => $request->html
            ];
            $banner->fill($data);
            $banner->save();
            return response([
                'success' => true,
                'data' => $banner,

            ]);
        } catch (\Throwable $th) {
            return $th;
            return response([
                'success' => false,
                'data' => $th->getMessage(),

            ]);
        }
    }
    public function bannerDetails($location)
    {
        $banner =  Banner::where('location', $location)->first();
        if (!$banner) {
            return response([
                'success' => false,
                'action' => 'createNew',
            ]);
        }
        return response([
            'success' => true,
            'data' => $banner,

        ]);
    }


    public function addUser(Request $request)
    {
        try {

            $user = ($request->id !== 'new' ? User::findOrFail($request->id) : new User(['id' => Str::uuid()]));
            // dd($user);
            $data = [

                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'api_token' => Str::random(60),
            ];
            $user->fill($data);
            $user->save();
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage(),
                ]
            ], 201);
        }
    }

    public function fileUpload(Request $request)
    {

        // return $favicon =  $request->file('favicon')->getClientOriginalName();
        if ($request->hasFile('favicon')) {
            $favicon =  $request->file('favicon');
            $filename =  'favicon-' . $favicon->hashName();
            Options::where('name', 'websiteFavicon')->update([
                'value' => $filename
            ]);

            try {
                $request->file('favicon')->storeAs('public', $filename);

                return response([
                    'success' => true,
                    'data' => [
                        'name' => 'favicon',
                        'file_name' => $filename

                    ]
                ], 200);
            } catch (\Throwable $th) {
                return response([
                    'success' => false,
                    'error' => [
                        'code' => $th->getCode(),
                        'msg' => $th->getMessage()
                    ]
                ], 200);
            }
        }

        if ($request->hasFile('featured-image')) {
            $favicon =  $request->file('featured-image');

            $filename =  $favicon->hashName();
            Options::where('name', 'websiteFeaturedImage')->update([
                'value' => $filename
            ]);

            try {
                $request->file('featured-image')->storeAs('public', $filename);

                return response([
                    'success' => true,
                    'data' => [
                        'name' => 'featured-image',
                        'file_name' => $filename
                    ]
                ], 200);
            } catch (\Throwable $th) {
                return response([
                    'success' => false,
                    'error' => [
                        'code' => $th->getCode(),
                        'msg' => $th->getMessage()
                    ]
                ], 200);
            }
        }
        if ($request->hasFile('featuredImagePrediksiHariIni')) {
            $favicon =  $request->file('featuredImagePrediksiHariIni');

            $filename =  $favicon->hashName();
            Options::where('name', 'featuredImagePrediksiHariIni')->update([
                'value' => $filename
            ]);

            try {
                $request->file('featuredImagePrediksiHariIni')->storeAs('public', $filename);

                return response([
                    'success' => true,
                    'data' => [
                        'name' => 'featuredImagePrediksiHariIni',
                        'file_name' => $filename
                    ]
                ], 200);
            } catch (\Throwable $th) {
                return response([
                    'success' => false,
                    'error' => [
                        'code' => $th->getCode(),
                        'msg' => $th->getMessage()
                    ]
                ], 200);
            }
        }
    }

    public function fileUploadGet()
    {
        return response([
            'websiteFavicon' => option('websiteFavicon'),
            'websiteFeaturedImage' => option('websiteFeaturedImage'),
            'featuredImagePrediksiHariIni' => option('featuredImagePrediksiHariIni'),
        ], 200);
    }
    public function changePassword(Request $request)
    {
        try {
            $data = User::findOrFail($request->uuid);
            $data->fill([
                'password' => Hash::make($request->new)
            ]);
            $data->save();
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 200);
        }
    }

    public function customLombaPost(Request $request)
    {
        try {
            $updateHeader = Pages::where('slug', 'custom-lomba-header')
                ->update(['html' => $request->header]);

            $updateFooter = Pages::where('slug', 'custom-lomba-footer')
                ->update(['html' => $request->footer]);
            return response([
                'success' => true,
                'data' => [
                    'header' => $updateHeader,
                    'footer' => $updateFooter,
                ]
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    public function customLombaGet()
    {
        try {
            $data = [
                'header' => loadPageBySlug('custom-lomba-header'),
                'footer' => loadPageBySlug('custom-lomba-footer')
            ];
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }


    public function customDukunGet()
    {
        try {
            $data = [
                'header' => loadPageBySlug('custom-dukun-header'),
                'footer' => loadPageBySlug('custom-dukun-footer')
            ];
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    public function customDukunPost(Request $request)
    {
        try {
            $updateHeader = Pages::where('slug', 'custom-dukun-header')
                ->update(['html' => $request->header]);

            $updateFooter = Pages::where('slug', 'custom-dukun-footer')
                ->update(['html' => $request->footer]);
            return response([
                'success' => true,
                'data' => [
                    'header' => $updateHeader,
                    'footer' => $updateFooter,
                ]
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }


    // 5 maret 2021
    public function enableAll()
    {
        try {
            $data =  DB::table('users')->where('level', 'user')->update(['status' => 'active']);

            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    public function deleteAll()
    {

        try {
            $data =  DB::table('users')->where('level', 'user')->delete();

            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
    public function disableAll()
    {

        try {
            $data =  DB::table('users')->where('level', 'user')->update(['status' => 'inactive']);

            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    public function disableUser(Request $request)
    {
        try {
            // $data =  DB::table('users')->where('id', $request->id)->update(['status' => 'active']);
            $data =  User::findOrFail($request->id);
            $data->status = 'inactive';
            $data->save();
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
    public function enableUser(Request $request)
    {
        try {
            // $data =  DB::table('users')->where('id', $request->id)->update(['status' => 'enable']);
            $data =  User::findOrFail($request->id);
            $data->status = 'active';
            $data->save();
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
}
