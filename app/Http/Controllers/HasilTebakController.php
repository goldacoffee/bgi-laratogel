<?php

namespace App\Http\Controllers;

use App\HasilTebak;
use Illuminate\Http\Request;

class HasilTebakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HasilTebak  $hasilTebak
     * @return \Illuminate\Http\Response
     */
    public function show(HasilTebak $hasilTebak)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HasilTebak  $hasilTebak
     * @return \Illuminate\Http\Response
     */
    public function edit(HasilTebak $hasilTebak)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HasilTebak  $hasilTebak
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HasilTebak $hasilTebak)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HasilTebak  $hasilTebak
     * @return \Illuminate\Http\Response
     */
    public function destroy(HasilTebak $hasilTebak)
    {
        //
    }
}
