<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        // dd(Auth::user()->level);
        if (Auth::user()->level === "user") {
            // redirect('/' . option('tebakSkorPermalink'));

            return redirect(route('tebaknomor'));
        } else {

            return view('dashboard');
        }
    }
}
