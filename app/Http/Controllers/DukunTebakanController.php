<?php

namespace App\Http\Controllers;

use App\DukunTebakan;
use Illuminate\Http\Request;

class DukunTebakanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DukunTebakan  $dukunTebakan
     * @return \Illuminate\Http\Response
     */
    public function show(DukunTebakan $dukunTebakan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DukunTebakan  $dukunTebakan
     * @return \Illuminate\Http\Response
     */
    public function edit(DukunTebakan $dukunTebakan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DukunTebakan  $dukunTebakan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DukunTebakan $dukunTebakan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DukunTebakan  $dukunTebakan
     * @return \Illuminate\Http\Response
     */
    public function destroy(DukunTebakan $dukunTebakan)
    {
        //
    }
}
