<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TogelRekomendasi extends Model
{
    protected $table = 'rekomendasi';
    protected $fillable = [
        'id',
        'nama',
        'deskripsi',
        'min_depo',
        'rating',
        'link',
        'position',
    ];
    protected $keyType = 'string';
    public $incrementing = false;

    protected $casts = [
        'id' => 'string',

    ];
    public $hidden = ['created_at', 'updated_at'];
}
