<?php

namespace App\Console\Commands;

use App\DukunHasil;
use App\DukunTebakan;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class DukunTogel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'togel:dukun {provider} {tanggal} {keluar}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hitung hasil dukun togel harian setelah keluaran provider berhasil diinput oleh pihak mafiahasil';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $provider = $this->argument('provider');
        $tanggal = $this->argument('tanggal');
        $keluar = $this->argument('keluar');
        $this->info($tanggal . ' - ' . $keluar);
        // $today = date('Y-m-d');

        $tebakanUser = DukunTebakan::where('tanggal', $tanggal)->get();
        // echo json_encode($tebakanUser);
        // die;



        //  delete data di hari itu
        // $del = DukunHasil::where('tanggal', $tanggal)
        //     ->where('provider', $provider)->first();
        // if (!$del) {
        //     $del->delete();
        // }


        // for real result
        // $keluaran = $this->getKeluaranData($provider);
        // $angkaKeluar = $keluaran ;

        // simulasi
        $angkaKeluar = str_pad($keluar, 4, "0", STR_PAD_LEFT);

        $result = [];
        foreach ($tebakanUser as $tebakan) {
            $tebakanUser =
                $this->hitungSkor($tebakan['tebakan'][ucwords($provider)], $angkaKeluar, $tebakan, ucwords($provider), $tanggal);

            array_push($result, $tebakanUser);
        }

        // return $result;

        echo json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);


        // $this->info($tebakanUser);
        //  $this->info($tebakanUser);

    }

    public function hitungSkor($tebakanUser, $angkaKeluar, $tebakan, $provider, $tanggal)
    {

        $colokBebas = $tebakanUser['colokbebas'];
        $kombinasi2D = $tebakanUser['kombinasi2d'];
        $kombinasiReal = $this->arrayKombonasi2D($kombinasi2D);




        $nilai = [
            'provider' => $provider,
            'keluar' => $angkaKeluar,
            'colokbebas' => $this->hitungSkorColokBebas($tebakanUser['colokbebas'], $angkaKeluar),
            'kombinasi' => $this->hitungSkorKombinasi($this->arrayKombonasi2D($tebakanUser['kombinasi2d']), $angkaKeluar),
        ];

        $nilai['total_skor'] = $nilai['colokbebas']['skor'] + $nilai['kombinasi']['skor'];



        $provider = strtolower($provider);

        // $fillData = [
        //     'id_tebakan' => $tebakan['id'],


        // ];

        // $x = new DukunHasil(['id' => Str::uuid()]);
        // $x->fill($fillData);
        // $x->save();

        // return $tebakan;
        $this->info('Input hasil tebakan provider: ' . ucwords($provider));
        $newUUID = Str::uuid();

        $inputData = [
            'id_tebakan' => $tebakan['id'],
            'tanggal' => $tanggal,
            'id' => $newUUID,
            'keluaran_' . $provider => str_pad($angkaKeluar, 4, "0", STR_PAD_LEFT),
            'id_user' => $tebakan['user_id'],
            'colokbebas_' . $provider => $nilai['colokbebas'],
            'kombinasi_' . $provider => $nilai['kombinasi'],
            'skor_' . $provider => $nilai['total_skor'],

        ];

        // DukunHasil::updateOrCreate([
        //     'id_tebakan' => $tebakan['id'],
        //     'tanggal' => $tanggal,
        // ], [
        //     'id' => $newUUID,
        //     'keluar' => $angkaKeluar,
        //     'id_user' => $tebakan['user_id'],
        //     'colokbebas_' . $provider => $nilai['colokbebas'],
        //     'kombinasi_' . $provider => $nilai['kombinasi'],
        //     'skor_' . $provider => $nilai['total_skor'],

        // ]);


        $x = DukunHasil::where(
            [
                ['id_tebakan', $tebakan['id']],
                ['tanggal', $tanggal]
            ]
        )->first();

        if ($x === null) {
            $hasil = DukunHasil::create($inputData);


            $update = DukunHasil::find($hasil->id);
            // return $update;
            $update->totalskor  =  $update->skor_sydney + $update->skor_hongkong;
            $update->save();

            return $update;
        } else {
            $x->update($inputData);
            $x->totalskor  =  $x->skor_sydney + $x->skor_hongkong;
            $hasil =  $x->save();


            return $hasil;
        }



        // $x =  DukunHasil::find($newUUID)->first();
        // $x->totalskor = $x->skor_sydney + $x->skor_hongkong;

        // $x->save();



        // $this->info('Input hasil tebakan provider: ' . ucwords($provider));

        // // return $x->totalskor .' ' . $x->skor_sydney . ' - ' . $x->skor_hongkong;
        // return $newUUID;
        // return $x;



        // print_r($fillData);
        //$this->info(json_encode($tebakanUser));
    }




    private function cariNilaiKombinasi($angkaKeluar)
    {
        $stmt = [];
        $stmt_kom = [];
        $angka3 = substr($angkaKeluar, 2);
        $angka4 = substr($angkaKeluar, 3);
        if ($angka3 < 50) {
            //masuk kecil
            $stmt[] .= 'Kecil';
            if ($angka3 % 2 == 0) {
                // habis dibagi 2 = genap
                $stmt_kom[] .= 'Kecil - Genap';
            } else {
                // kecil ganjil
                $stmt_kom[] .= 'Kecil - Ganjil';
            }
        } else {
            // masuk besar
            $stmt[] .= 'Besar';
            if ($angka3 % 2 == 0) {
                // masuk besar genap
                $stmt_kom[] .= 'Besar - Genap';
            } else {
                // masuk besar ganjil
                $stmt_kom[] .= 'Besar - Ganjil';
            }
        }

        return $stmt_kom[0];
    }


    private function arrayKombonasi2D($index)
    {
        $arr = [
            'Besar - Genap',
            'Besar - Ganjil',
            'Kecil - Genap',
            'Kecil - Ganjil',
        ];

        return $arr[$index];
    }

    public function hitungSkorColokBebas($tebakanUser, $angkaKeluar)
    {


        $skor = 0;
        for ($i = 0; $i < 4; $i++) {
            if ($tebakanUser == $angkaKeluar[$i]) {
                $skor = $skor + 10;
            }
        }
        if ($skor == 0) {
            $keterangan = '(X = Poin 0)';
        } else {
            $keterangan = "(JP = Poin +$skor)";
        }

        return [
            'tebakan' => $tebakanUser,
            // 'keluar' => $angkaKeluar,
            'skor' => $skor,
            'keterangan' => $keterangan
        ];
    }
    public function hitungSkorKombinasi($tebakanUser, $angkaKeluar)
    {

        $nilaiKombinasi = $this->cariNilaiKombinasi($angkaKeluar);

        return [
            'keluar' => $nilaiKombinasi,
            'tebakan' => $tebakanUser,
            'skor' => ($nilaiKombinasi ==  $tebakanUser) ? '10' : 0,
            'keterangan' => ($nilaiKombinasi ==  $tebakanUser) ? '(JP = Poin +10)' : '(X = Poin 0)'
        ];
    }



    private function getKeluaranData($provider)
    {
        // return option('endPointKeluaranPerProvider') . $keluaran;
        if (option('status') === 'local') {

            return json_decode(file_get_contents(public_path() . '/json/hongkong.json'), true);
        } else {
            return json_decode(@file_get_contents(option('endPointKeluaranPerProvider') . $provider), true);


            // 'https://mafiahasil.top/api/data/all.json'
        }
    }
}
