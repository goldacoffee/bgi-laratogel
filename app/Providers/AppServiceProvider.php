<?php

namespace App\Providers;

use App\DukunHasil;
use App\DukunTebakan;
use App\Pages;
use App\TebakanUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // \App\Pages::creating(function ($pages) {
        //     $pages->slug = ($pages->slug == '' ?  Str::slug($pages->title, '-') : Str::slug($pages->slug));
        // });
        // \App\Pages::updating(function ($pages) {
        //     // $pages->slug = Str::slug($pages->title, '-');
        //     $pages->slug = ($pages->slug == '' ?  Str::slug($pages->title, '-') : Str::slug($pages->slug));
        //     // $pages->slug = ($pages->slug == ''  ?  Str::slug($pages->title, '-') : $pages->slug);
        // });

        \App\PeriodeTebak::deleting(function ($periodeTebak) {
            $tebakan = TebakanUser::where('id_periode', $periodeTebak->id);
            $tebakan->delete();
        });


        \App\DukunPeriode::deleting(function ($dukunPeriode) {
            $id_periode = $dukunPeriode->id;
            $idDelete = DukunTebakan::where('periode_id', $id_periode)->pluck('id');
            $dukunHasilDelete = DukunHasil::whereIn('id_tebakan', $idDelete)->delete();
            Log::info('Delete table dukun_hasil data with ID: ' . $id_periode . ' invoked by deleting periode ');
            Log::info(json_encode($dukunHasilDelete));


            $tebakan = DukunTebakan::where('periode_id', $id_periode)->delete();
            Log::info('Delete table dukun_tebakan data with ID: ' . $id_periode . ' invoked by deleting periode ');
            Log::info(json_encode($tebakan));
        });

        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');
    }
}
