<?php

use App\Pages;
use App\Banner;
use App\DukunPeriode;
use App\Menu;
use App\Options;
use App\Sidebar;
use App\TogelRekomendasi;
use App\PeriodeTebak;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;

function option($name)
{
    try {
        $data = Options::firstOrCreate(
            ['name' => $name],
            ['value' => $name]
        );
        return $data->value;
    } catch (\Throwable $th) {
        return $name;
    }
}


function jsonRender($json)
{
    // return json_decode($json, true);
    return (new \Scrumpy\ProseMirrorToHtml\Renderer)->render(json_decode($json, true));
}


function loadSidebar()
{
    return Sidebar::all();
}

function loadCustomFooter()
{
    $data = Pages::where('slug', 'custom-footer')->first();
    if ($data) {
        return $data->html;
    }
}
function loadCustomNav()
{

    return $menu = option('custom_menu');
}

function loadMenu()
{

    return $menu = Menu::orderBy('position', 'ASC')->get();
}
function loadBanner($location)
{
    $data =  Banner::where('location', $location)->first();
    if ($data) {
        return $data->html;
    }
}
function loadFooterBanner()
{
    $data =  Banner::where('location', 'floatin')->get();
    return $data;
}

function loadCustomCSS()
{
    $css =  option('custom_css');
    if ($css) {
        return '<style>' . $css . '</style>';
    }
}
function loadCustomJs()
{
    $js =  option('custom_js');
    if ($js && Route::currentRouteName() !== 'dashboard') {

        return $js;
    }
}

function clearNL($string)
{

    $string =  str_replace("\n", '', $string);
    $string =  nl2br($string);
    $string = strip_tags($string);

    return trim(preg_replace('/\s+/', ' ', $string));
}

function buildTitle($routename)
{
    // return $param;
    // $routename = Route::currentRouteName();
    $meta = [];
    switch ($routename) {
        case 'home':
            $title =  option('homepageTitle') . ' ' . option('titleSeparator') . ' ' . option('websiteName');
            $description = option('websiteDescription');
            $url = option('websiteDomain');
            $featuredImage = (strpos(option('websiteFeaturedImage'), 'http') !== false) ? asset('/storage/' . option('websiteFeaturedImage')) : option('websiteFeaturedImage');
            $featuredImage = option('websiteFeaturedImage');

            $keywords = option('websiteKeyword');

            break;
        case 'livedraw-index':
            $title =  option('livedrawPageTitle') . ' ' . option('titleSeparator') . ' ' . option('websiteName');
            $description = (option('livedrawPageDescription') !== '' ? option('livedrawPageDescription') : option('websiteDescription'));
            $url = url()->current();
            $featuredImage = (option('livedrawFeaturedImage') !== '' || option('livedrawFeaturedImage') !== 'livedrawFeaturedImage' ? option('livedrawFeaturedImage') : asset('/storage/' . option('websiteFeaturedImage')));
            $keywords = option('livedrawKeyword');

            break;
        case 'datakeluaran':
            $param = Route::current()->parameters();
            $tanggal = date('m-d-Y', strtotime(now()));

            // dd($providerName);
            // dd($param);
            // dd($param['providername']);
            // dd($param->providername);
            $providerName = (string) $param['providername'];
            $title =  sprintf(option('titlePageKeluaranPerProvider'), ucwords($param['providername'])) . ' ' . option('titleSeparator') . ' ' . option('websiteName');

            $description = option('websiteDescription');
            $url = option('websiteDomain');
            $featuredImage = option('websiteFeaturedImage');
            $keywords = sprintf(option('keywordPageKeluaranPerProvider'), $param['providername']);

            break;
        case 'dashboard':
            return  '<title>BGI-Togel Dashboard</title>';
            break;

            break;
        case 'prediksi.tomorrow':
            $param = Route::current()->parameters;
            $titleArray = [sprintf(option('titlePagePrediksiPerProvider'), $param['providername']), $param['hari'], $param['tanggal']];
            $title =  ucwords(implode(' ', $titleArray)) . ' ' . option('titleSeparator') . ' ' . option('websiteName');
            $description = sprintf(option('descriptionPagePrediksiKeluaranPerProvider'), $param['providername']);
            $url = Url()->current();
            $featuredImage = option('websiteFeaturedImage');
            $keywords = sprintf(option('keywordPagePrediksiKeluaranPerProvider'), $param['providername']);

            break;
        case 'gotoPage':
            if (Route::current()->parameters) {
                $data = Pages::where('slug', Route::current()->parameters['value'])->first();
                $title = $data->title . ' ' . option('titleSeparator') . ' ' . option('websiteName');
                // $description = Str::limit(clearNL($data->html), 150);
                $description = ($data->description == null || $data->description == ''  ?  option('websiteDescription') : $data->description);
                $url = route('gotoPage', $data->slug);
                $featuredImage = $data->featured_image;
                $keywords = $data->title . ',' .  option('websiteKeyword');
            } else {
                return '';
            }


            break;
        case 'prediksitogel':

            $title = option('titlePagePrediksiHariIni') . ' ' . option('titleSeparator') . ' ' . option('websiteName');
            $description = option('deskripsiPagePrediksiHariIni');
            $keywords = option('websiteKeyword');
            $url = url()->current();
            $featuredImage =  option('featuredImagePrediksiHariIni');

            break;
        case 'tebaknomor':
            $title = option('titleTebakNomor') . ' ' . option('titleSeparator') . ' ' . option('websiteName');
            $description = option('deskripsiTebakNomor');
            $keywords = option('websiteKeyword');
            $url = url()->current();
            $featuredImage = option('featuredImageTebakNomor');
            break;
        case 'dukuntogel':
            $title = option('titleDukunTogel') . ' ' . option('titleSeparator') . ' ' . option('websiteName');
            $description = option('deskripsiDukunTogel');
            $keywords = option('websiteKeyword');
            $url = url()->current();
            $featuredImage = option('featuredImageDukunTogel');
            break;
        default:

            return $routename;
            break;
    }
    $meta[] = '<link rel="icon" type="image/png" href="' . (Str::contains(option('websiteFavicon'), 'http') ? option('websiteFavicon') : asset('/storage/' . option('websiteFavicon')))  . '">';
    $meta[] = '<!-- Primary Meta Tags -->';
    $meta[] = '<title>' . $title . '</title>';
    $meta[] = '<meta name="title" content="' . $title . '">';
    $meta[] = '<meta name="description" content="' . $description . '">';
    $meta[] = '<meta name="keywords" content="' . $keywords . '">';

    $meta[] = '<!-- Open Graph / Facebook -->';
    $meta[] = '<meta property="og:type" content="website">';
    $meta[] = '<meta property="og:url" content="' . $url . '">';
    $meta[] = '<meta property="og:title" content="' . $title . '">';
    $meta[] = '<meta property="og:description" content="' . $description . '">';
    $meta[] = '<meta property="og:image" content="' . $featuredImage . '">';

    $meta[] = '<!-- Twitter -->';
    $meta[] = '<meta property="twitter:card" content="summary_large_image">';
    $meta[] = '<meta property="twitter:url" content="' . $url . '">';
    $meta[] = '<meta property="twitter:title" content="' . $title . '">';
    $meta[] = '<meta property="twitter:description" content="' . $description . '">';
    $meta[] = '<meta property="twitter:image" content="' . $featuredImage . '">';
    return implode("\n", $meta);
}


function  loadTop3Bandar()
{
    $data = TogelRekomendasi::orderBy('position', 'ASC')->limit(3)->get();
    return $data;
}

function formatDate($originalDate)
{

    return date("d-m-Y", strtotime($originalDate));
}

function loadPageBySlug($slug)
{
    try {
        $data = Pages::where('slug', $slug)->first();
        return $data->html;
    } catch (\Throwable $th) {
    }
}

function loadPageSlug($id)
{
    try {
        $slug = Pages::findOrFail($id)->slug;
        return $slug;
    } catch (\Throwable $th) {
        return $id;
    }
}

function currentDomain()
{
    return option('websiteDomain') . '/';
}

function permalinKeluaran($keluaran)
{
    return currentDomain() . option('gotoProviderPermalinkPrefix') . '-' . strtolower($keluaran)  . option('gotoProviderPermalinkSuffix');
}
function permalink($name, $param = '')
{
    return slugify(option($name) . ' ' .  str_replace(' ', '-', trim($param)));
}


function permalinkTomorrow($provider, $hari, $tanggal)
{
    $array = [
        strtolower($provider),
        strtolower($hari),
        $tanggal
    ];
    return vsprintf(option('gotoProviderPermalinkTomorrow') . "-%s-%s-%s", $array);
}

function listMenu()
{
    return [
        'hongkong' => [
            'title' => 'Hongkong',
            'description' => "Keluaran Togel %s Hari Ini"
        ],
        'singapore' => [
            'title' => 'Hongkong',
            'description' => "Keluaran Togel %s Hari Ini"
        ],
        'beijing' => [
            'title' => 'Beijing',
            'description' => "Keluaran Togel % Hari Ini"
        ],


    ];
}

function jsonLD_breadcumb($target, $param = '')
{

    $menu =  listMenu();
    if ($target == 'content') {
        $param = ucwords($param);
        $result =  [
            "@context" => "https://schema.org",
            "@type" => "BreadcrumbList",
            "itemListElement" => [
                "@type" => "ListItem",
                "position" => 1,
                "item" => [
                    "@id" => url()->current(),
                    "name" => sprintf(option('titlePageKeluaranPerProvider'), ucwords($param))
                ]
            ]

        ];
    }

    return json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
}

function jsonLD_home()
{
    $menu =  listMenu();
    $arrayMenu = [];
    $x = 1;
    foreach ($menu as $key => $value) {
        array_push($arrayMenu, [
            "@type" => "SiteNavigationElement",
            "position" => $x++,
            "name" => $value['title'],
            "description" => sprintf($value['description'], ucwords($key)),
            "url" => permalinKeluaran($key)
        ]);
    }

    $result =  [
        "@context" => "https://schema.org",
        "@type" => "ItemList",
        "itemListElement" => $arrayMenu
    ];

    // $name = [];
    // $link = [];

    // foreach ($menu as $key => $value) {
    //   $name[] = $value['title'];
    //   $link[] = permalinKeluaran($key);
    // }
    // $result =  [
    //   "@context":"http://schema.org",
    // "@type":"ItemList",
    //   "name" => $name,
    //   "url" => $link
    // ];



    return json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
}


function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}


function allProvider()
{

    // if (env('APP_ENV') !== 'local') {
    if (option('status') === 'local') {

        return json_decode(file_get_contents(public_path() . '/json/allx.json'), false);
    } else {
        return json_decode(@file_get_contents(option('endPointKeluaranAll')), false);


        // 'https://mafiahasil.top/api/data/all.json'
    }
}

function linktoProvider($kode)
{
}



function todayHariTanggal($param)
{
    return implode(', ', detectHariEsok($param->tanggal));
    // dd();

    // $array_hari = [];
    // $array = [
    //     "hari" =>  namaHari(date('D', strtotime(now()))),
    //     "tanggal" => implode('-', array_reverse(explode('-', date('Y-m-d', strtotime(now()))))),
    // ];
    // return implode(', ', $array);
}

function detectHariEsok($tanggal)
{
    $date1 = str_replace('-', '/', $tanggal);
    $hariLengkap = date('Y-m-d', strtotime($date1 . "+1 days"));
    $hari = date('D', strtotime($date1 . "+1 days"));
    // $hari =  date("D", strtotime($tanggal + 1));
    // return $hari;

    switch ($hari) {
        case 'Sun':
            $hari_ini = "Minggu";
            break;

        case 'Mon':
            $hari_ini = "Senin";
            break;

        case 'Tue':
            $hari_ini = "Selasa";
            break;

        case 'Wed':
            $hari_ini = "Rabu";
            break;

        case 'Thu':
            $hari_ini = "Kamis";
            break;

        case 'Fri':
            $hari_ini = "Jumat";
            break;

        case 'Sat':
            $hari_ini = "Sabtu";
            break;

        default:
            $hari_ini = "Tidak di ketahui";
            break;
    }
    return [
        'hari' => $hari_ini,
        'tanggal' => implode('-', array_reverse(explode('-', $hariLengkap)))
    ];
    // return $hari_ini;
}

function detectHariEsokSpecial($provider, $tanggal)
{
    // dd($tanggal);
    // $date1 = str_replace('-', '/', $tanggal);
    // $hariLengkap = date('Y-m-d', strtotime($tanggal,));
    // // dd($hariLengkap);
    // // $hari = date('D', strtotime($tanggal));

    // $hari =  date("D", strtotime($tanggal + 1));

    $hariLengkap = date('Y-m-d', strtotime($tanggal . "+1 days"));
    $hari = date('D', strtotime($tanggal . "+1 days"));
    // return $hari;

    switch ($hari) {
        case 'Sun':
            $hari_ini = "Minggu";
            break;

        case 'Mon':
            $hari_ini = "Senin";
            break;

        case 'Tue':
            $hari_ini = "Selasa";
            break;

        case 'Wed':
            $hari_ini = "Rabu";
            break;

        case 'Thu':
            $hari_ini = "Kamis";
            break;

        case 'Fri':
            $hari_ini = "Jumat";
            break;

        case 'Sat':
            $hari_ini = "Sabtu";
            break;

        default:
            $hari_ini = "Tidak di ketahui";
            break;
    }
    $arr =  [
        'providername' => $provider,
        'hari' => strtolower($hari_ini),
        'tanggal' => implode('-', array_reverse(explode('-', $hariLengkap)))
    ];
    return $arr;

    // return implode(', ', $arr);


    // return $hari_ini;
}


function getCurrentPeriod()
{
    try {
        $lastperiode =  PeriodeTebak::where('status', '1')
            ->orderBy('created_at', 'DESC')->first();

        return 'Periode : ' . $lastperiode->nama;
    } catch (\Throwable $th) {
        return '';
    }
}
function getDukunTogelCurrentPeriode()
{
    try {
        $lastperiode =  DukunPeriode::orderBy('created_at', 'DESC')->first();

        return 'Periode : ' . $lastperiode->name;
    } catch (\Throwable $th) {
        return '';
    }

    return 'currentperiodenotset';
}
